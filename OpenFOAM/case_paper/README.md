## READ ME
このディレクトリは過去に論文として出版された内容のケースディレクトリ  
In this directory, the case directories of OpenFOAM, which was published in papers, are placed.  

### ultrasound
超音波に関する論文でのケースディレクトリ  
Case directories of ultrasonic papers

