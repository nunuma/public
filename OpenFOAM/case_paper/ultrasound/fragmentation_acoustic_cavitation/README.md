## READ ME

### ケースディレクトリ(case directory)  

各種音圧振幅におけるケースディレクトリ  
case directories with different pressure amplitudes  
論文中ではOpenFOAMのversionはESI ver. v1812を利用したが、v2212以降にCSFモデルの曲率計算時にスムーザーを標準実装したため、このケースディレクトリはv2212用に変更している。  


### 実行方法 (execution method)  

実行方法は以下のとおり。論文中ではAdaptive Mesh Refinementを利用したものの、計算負荷が重たくなるだけであったので、このケースでは局所細分割をrefineMeshで実行した。  
The execution method is followings. In the paper, Adaptive Mesh Refinement was incorporated in the method, although this case does not include this method because the calculation time is longer with AMR. Instead, refineMesh is used to use local fine computational grid.  

```
blockMesh  
topoSet  
refineMesh  
renumberMesh -overwrite  
setFields  
decomposePar  
mpirun -np * compressibleInterFoam -parallel  
reconstructPar  
```
