## READ ME

### ケースディレクトリ(case directory)  

各種気泡配置におけるケースディレクトリ  
case directories with different initial bubble locations    
論文中で利用したOpenFOAMのversionはv1812であったが、v2212以降にCSFモデルの曲率計算時にスムーザーを標準実装したため、このケースディレクトリはv2212用に変更している。  
In my paper, the used OpenFOAM version is v1812. But, I changed the case directory for v2212 because the smoother at the calculation of surface curvature was implemented into OpenFOAM v2212.  
このケースディレクトリは圧力振幅が0.5気圧、周囲気泡は正六面体、周囲気泡との距離は150 microの条件である。  


### 実行方法 (execution method)  

実行方法は以下のとおり。  
The execution method is followings.   

```
cp -r 0.orig 0
blockMesh  
renumberMesh -overwrite  
setFields  
decomposePar  
mpirun -np * compressibleInterDyMFoam -parallel  
reconstructParMesh 
reconstructPar    
```

###  条件の変更方法(To change calculation conditions)   
論文中では、音圧振幅と初期配置を変更している。音圧振幅は境界条件を変更する、初期配置はsetFieldsDictを変更し、setFieldsを実行することで変更することができる。例として、以下に圧力(p_rgh)の境界条件の設定箇所を示す。  
```
    bottom
    {
        type            uniformFixedValue;
        uniformValue
        {
            type             sine;
            frequency        2e4;
            amplitude        -5e4;
            scale            1;  // Scale factor for wave
            level            1e5;  // Offset
        }
    }
```
全境界で同様の境界条件を設定しているが、圧力を正弦波で与えるために、uniformFixedValueを指定する。そして、typeにsineを指定することで、正弦波を与える境界条件を設定する。frequencyは周波数であり、この条件では20 kHzの周波数の圧力波を与えることに対応する。amplitudeは圧力振幅であり、ここでは0.5気圧の圧力振幅を与えることに対応する。levelはベースラインとなる圧力であり、ここを1気圧に設定することで、1気圧条件下での振幅0.5気圧、20 kHzの圧力振動を与える境界条件となっている。論文中では圧力振幅を0.5気圧から0.9気圧まで変更しているので、このamplitudeを5e4から9e4まで変更すれば論文と同様に圧力振幅を変更することができる。  


気泡の初期条件の変更のためのsetFieldsDictの一部を示す。  
```
    sphereToCell
    {   
        centre  (0 0 0);
        radius  1e-5;
        fieldValues
        (   
            volScalarFieldValue alpha.water 0 // Air
            volScalarFieldValue p 114400
            volScalarFieldValue p_rgh 114400
        );
    }
    sphereToCell
    {   
        centre  (8.6603e-5 8.6603e-5 8.6603e-5);
        radius  1e-5;
        fieldValues
        (   
            volScalarFieldValue alpha.water 0 // Air
            volScalarFieldValue p 114400
            volScalarFieldValue p_rgh 114400
        );
    }
...
```
setFieldsのsphereToCellという機能を利用し、球形領域に初期条件を与える。ここでは、alpha.waterを0にすることで球形領域に気相を与え、Laplace圧分の圧力を増加させた圧力をこちらで与える。   
このsphereToCellが気泡の個数分設定する。centreが気泡中心を表すので、論文中で行ったように正多面体の頂点の位置座標をこのcentreに入れることで、正多面体の頂点の位置に気泡を配置することができる。論文中では、正四面体、正六面体、正八面体の条件で距離を変更して計算をしているが、setFieldsDictをそれぞれ変更して、正多面体の頂点に気泡を配置した後計算する。  
 
参考までに、気泡距離が150 micro、正四面体、正八面体での気泡配置のsetFieldsDictをこちらに置いておく。   
