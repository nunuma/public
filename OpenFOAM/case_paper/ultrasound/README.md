## READ ME

### fragmentation_acoustic_cavitation
音響キャビテーションの分裂に関する研究  
Fragmentation of acoustic cavitation bubble  


* 論文情報(Paper information)  
T. Yamamoto, S.-i. Hatanaka, S. V. Komarov, Fragmentation of cavitation bubble in ultrasound field under small pressure amplitude, Ultrasonics Sonochemistry, 58 (2019) 104684.    
[Link](https://doi.org/10.1016/j.ultsonch.2019.104684)  

### enhancement_cloud  
音響キャビテーション気泡クラウドでの振動振幅増幅に関する研究  
Enhancement of oscillation amplitude of acoustic cavitation in a cloud  


* 論文情報(Paper information)  
T. Yamamoto, S. V. Komarov, Enhancement of oscillation amplitude of cavitation bubble due to acoustic wake effect in multibubble environment, Ultrasonics Sonochemistry, 78 (2021) 105734.  
[Link](https://doi.org/10.1016/j.ultsonch.2021.105734)  
