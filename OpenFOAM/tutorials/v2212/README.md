# README #


### OpenFOAM tutorial cases for improved solvers for v2212

* * *

## helmholtzFoam

Tutorial test cases for helmholtzFoam.  
ヘルムホルツ方程式を解くためのsolver (helmholtzFoam)用tutorial  
以下のヘルムホルツ方程式を解く  

![ヘルムホルツ](./helmholtz.png)

ここで、Pは音圧振幅、kは波数である。  

OpenFOAMの設定ファイルとして0, constant, systemがあるが、0の中には音圧振幅であるP、constantの中には波数kを設定するtransportPropertiesが入っている。この辺りのファイルを編集することで、計算条件を変更することができる。   

* 0/P  
ここでは、初期条件、境界条件の設定を行う。初期条件は均一音圧場で構わない。収束性が悪い場合は初期音圧場を与えると収束性が改善することがある。以下に記載例を示す。
```
    top
    {
        type            fixedValue;
        value           uniform  1e4;
    }

    fixedWalls
    {
        type            fixedValue;
        value           uniform  0;
    }

    bottom 
    {
        type            zeroGradient;
    }
```
このtutorialでは、境界条件としてfixedValueを指定しているが、zeroGradientも利用できる。fixedValueで値を指定した場合、音源の音圧振幅を与える。もしくは、fixedValueでuniform 0を指定し、完全に吸音する条件を課すこともできる。zeroGradientを指定した場合は完全に音波が反射する条件となる。      

* constant/transportProperties  
ここでは、波数を設定する。以下に記載例を示す。  
```
k              k [ 0 -1 0 0 0 0 0 ] 41.8879; // k=omega/c (10 kHz,water)
```
ここでは、波数kのみ設定する。波数は角周波数を音速で割ったものである。水の音速は1500 m/sなので、10 kHzの場合、2 * pi * 10000 / 1500となる。　　 


* * *
## helmholtzBubbleFoam

Tutorial test cases for helmholzBubbleFoam.  
気泡が均一に分散している状態のhelmholz方程式を解くためのsolver (helmholtzBubbleFoam)用tutorial  
以下のヘルムホルツ方程式を解く  

![ヘルムホルツ_気泡](./helmholtzBubble.png)

ここで、kmは複素波数である。複素波数は以下のように表される。  

![複素波数](./km.png)

ここで、omegaは角周波数、cは音速、Nは単位体積あたりの気泡の個数、R0は平衡気泡半径、omega0は気泡の共振周波数、bは減衰係数である。   
共振周波数は以下のように表される。  

![共振周波数](./resonant.png)

ここで、sigmaは表面張力、p0は気泡内部の圧力であり、平衡気圧とラプラス圧を足したものである。

![気泡内部圧力](./p0.png)

また、Phiは複素無次元パラメータであり、以下のように表される。  

![複素無次元パラメータ](./phi.png)

ここで、gammaは比熱比、chiは次に表せられる無次元パラメータである。  

![無次元パラメータ](./chi.png)

ここで、Dは気相の熱拡散率である。  
また、減衰係数bは以下のように表される。  

![減衰係数](./damping.png) 

ここで、muは粘度である。  

上記のヘルムホルツ方程式を解く必要があるが、OpenFOAMでは虚数を利用することができない。このため、以下のように実数部と虚数部に分けて考える。  

![分割](./sep.png)  

また、波数も実数部と虚数部に分割する。  

![波数分割](./ksep.png)

これらをヘルムホルツ方程式に代入すると、以下のように実数部と虚数部の方程式に分割できる。  

![ヘルムホルツ分割1](./helmholtz1.png)

![ヘルムホルツ分割2](./helmholtz2.png)

JamshidiのPh.D. Thesisに従った記述で記載すると複素波数は以下のように記述できる。  

![複素波数分割](./km_sep.png)

ここで、betaは気泡の体積分率である。   

![気泡体積分率](./beta.png)

ここで、A, Bは以下のように記述できる。

![A](./A.png)

![B](./B.png)

helmholtzBubbleFoamではこの分割したヘルムホルツ方程式を解く。ここに配置しているtutorialでは、0, constant, systemが配置されているが、0の中ではPiとPrという変数が入っている。Prは上記の実数部分の音圧振幅、Piは虚数部分の音圧振幅を表す。Prの記述方法例は以下のとおりである。
```
    top
    {
        type            fixedValue;
        value           uniform  0;
    }
    sides
    {
        type            zeroGradient;
    }
    bottomIn
    {
        type            fixedValue;
        value           uniform  1e4;
    }
```
ここでは、実数部分の音圧振幅を設定する。実数部分の音圧なので、ここでは与える音圧振幅を設定する。この境界条件はhelmholtzFoamの音圧振幅の境界条件と同様である。   
虚数部分の音圧振幅も同様に設定する。  

constant/transportPropertiesでは、上記のomega/cをk0として、上記のA, B, betaを設定する。  
```
k0             k0 [ 0 -1 0 0 0 0 0 ] 104.71975; // k=omega/c (25 kHz,water)
A              A [ 0 0 0 0 0 0 0 ] 1e-5;
B              B [ 0 0 0 0 0 0 0 ] 4e-6;
beta           beta [ 0 0 0 0 0 0 0 ] 0;
```
betaを0とすると、気泡が分散していない系になるので、helmholtzFoamの結果と同様になる。  
A, Bの値は適宜条件に合わせて計算した上で入力する。  
