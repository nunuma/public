import pandas as pd
import numpy as np

# set parameters
f = 150.0e3
c = 1500.0
mu = 1.00e-03
rho = 1000.0
r0 = 5.0e-06
p0 = 1.00e5
omega = 2*np.pi*f
sigma = 7.25e-02
gamma = 1.4
D = 2.074e-05

# calculate parameters
xi = D/(omega*r0**2.0)
Phi = 3.0*gamma/(1.0 - 3.0*(gamma - 1.0)*1j*xi*((1j/xi)**0.5/np.tanh((1j/xi)**0.5) - 1.0))
b = 2.0*mu/(rho*r0**2.0) + p0/(2.0*rho*omega*r0**2.0)*(Phi).imag + r0*omega**2.0/(2.0*c)
omega0_2 = p0/(rho*r0**2.0)*((Phi).real - 2.0*sigma/(r0*p0))
A = (omega0_2 - omega**2.0)/(3.0*(c/r0)**2.0)
B = 2.0*b*omega/(3.0*(c/r0)**2.0)

# print calculated parameters
print("A = ")
print(A)
print("B = ")
print(B)
