# READ ME  

## solvers  
customized solvers are in this folder  

## tutorials  
tutorial cases for customized solvers are in this folder  

## pyFoam  
sample program to use pyFoam  

## case_paper  
case directory used in my previous papers
