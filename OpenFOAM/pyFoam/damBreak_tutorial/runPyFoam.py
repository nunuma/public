from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner

caseName = 'damBreakWithObstacle'

with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']

# copy 0.orig into 0
copyRun = BasicRunner(['cp','-r',caseName+'/0.orig',caseName+'/0'], silent=True)
copyRun.start()

# create base computational grid using blockMesh
meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
meshRun.start()

# identify the local zone using topoSet
topoSetRun = BasicRunner(['topoSet', '-case',caseName], silent=True)
topoSetRun.start()

# create wall zone by subsetting computational domain using subsetMesh 
subSetRun = BasicRunner(['subsetMesh','-overwrite','c0','-patch','walls','-case',caseName], silent=True)
subSetRun.start()

# create water zone using setFields
setFieldsRun = BasicRunner(['setFields','-case',caseName], silent=True)
setFieldsRun.start()

# execute solver
runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()
