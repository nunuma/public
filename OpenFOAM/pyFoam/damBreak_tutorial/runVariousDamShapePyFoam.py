# import classes from python library
import numpy as np
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Execution.BasicRunner import BasicRunner

# set the various dam shape
x_min = 0.6
x_max = 0.8
x_step = 2
y_min = 0.1875
y_max = 0.25
y_step = 2
z_min = 0.5
z_max = 0.75
z_step = 2
x_dam = np.linspace(x_min,x_max,x_step)
y_dam = np.linspace(y_min,y_max,y_step)
z_dam = np.linspace(z_min,z_max,z_step)

# set number of processors
nProc = 16

# template case
templateCase = SolutionDirectory('damBreakWithObstacle', archive=None)

# copy 0.orig into 0
copyRun = BasicRunner(['cp','-r','damBreakWithObstacle/0.orig','damBreakWithObstacle/0'], silent=True)
copyRun.start()

# roop with various dam shape
for ix in x_dam:
   for iy in y_dam:
      for iz in z_dam:

         # create new directory
         case = templateCase.cloneCase('damBreakWithObstacle-x_y_z_' + str(ix) + '_' + str(iy) + '_' + str(iz))
         caseName = 'damBreakWithObstacle-x_y_z_' + str(ix) + '_' + str(iy) + '_' + str(iz)

         # print case name
         print(caseName)

         # read solver name
         with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
            app = controlDict.content['application']

         # update setFieldsDict
         with ParsedParameterFile(caseName+'/system/setFieldsDict') as setFieldsDict:
            setFieldsDict["regions"] = ['boxToCell', {'box': '(0 0 0)' '('+str(ix)+' '+str(iy)+' '+str(iz)+')', 'fieldValues': ['volScalarFieldValue', 'alpha.water', 1]}]
            setFieldsDict.writeFile()

         # update number of processors in decomposeParDict
         with ParsedParameterFile(caseName+'/system/decomposeParDict') as decomposeParDict:
            decomposeParDict["numberOfSubdomains"] = str(nProc)
            decomposeParDict.writeFile()

         # create base computational grid using blockMesh
         meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
         meshRun.start()

         # identify the local zone using topoSet
         topoSetRun = BasicRunner(['topoSet', '-case',caseName], silent=True)
         topoSetRun.start()

         # create wall zone by subsetting computational domain using subsetMesh 
         subSetRun = BasicRunner(['subsetMesh','-overwrite','c0','-patch','walls','-case',caseName], silent=True)
         subSetRun.start()

         # create water zone using setFields
         setFieldsRun = BasicRunner(['setFields','-case',caseName], silent=True)
         setFieldsRun.start()

         # decompose calculation domain for parallel simulation
         DecomposeRun=BasicRunner(argv=["decomposePar","-case",caseName],logname="Decompose",silent=True)
         DecomposeRun.start()

         # execute solver
         runner = BasicRunner(['mpirun','-np',str(nProc),app,'-case',caseName,'-parallel'], silent=True)
         runner.start()

         # reconstruct the calculation domain
         ReconstructMeshRun=BasicRunner(argv=["reconstructParMesh","-case",caseName],logname="ReconstructMesh",silent=True)
         ReconstructMeshRun.start()

         # reconstruct the calculation domain
         ReconstructRun=BasicRunner(argv=["reconstructPar","-case",caseName],logname="Reconstruct",silent=True)
         ReconstructRun.start()
