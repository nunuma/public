# READ ME  


## runPyFoam.py  
damBreakWithObstacle tutorialをpyFoamで実行するプログラム。  
このコードでは最初にPyFoamの中のParsedParameterFileというライブラリとBasicRunnerというライブラリを読み出している。  
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
```

このParsedParameterFileというライブラリでは、OpenFOAMの設定ファイル等を変更したり、読み出したりすることができる。BasicRunnerではsolverの実行に利用されるライブラリである。  

最初にtutorialケースの名前を設定している。
```
caseName = 'damBreakWithObstacle'
```

次にParsedParameterFileの機能を利用し、caseNameで設定したフォルダのsystem/controlDictの場所を指定し、controlDictからapplicationを読み出している。  
```
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']
```
次に、BasicRunnerで実行するコマンドを設定し、BasicRunnerで実行する。ここでは、保存用の0ディレクトリ(0.orig)を0ディレクトリにコピーする。    
```
copyRun = BasicRunner(['cp','-r',caseName+'/0.orig',caseName+'/0'], silent=True)
copyRun.start()
```
次に、blockMeshをBasicRunnerで実行し、ベースとなる計算格子を作成する。
```
meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
meshRun.start()
```
次に、topoSetをBasicRunnerで実行し、計算領域の一部を領域指定する。
```
topoSetRun = BasicRunner(['topoSet', '-case',caseName], silent=True)
topoSetRun.start()
```
次に、topoSetで指定した領域をsubsetMeshでくり抜く。
```
subSetRun = BasicRunner(['subsetMesh','-overwrite','c0','-patch','walls','-case',caseName], silent=True)
subSetRun.start()
```
そして、計算開始前に、setFieldsで初期のダム形状を作成する。
```
setFieldsRun = BasicRunner(['setFields','-case',caseName], silent=True)
setFieldsRun.start()
```
最終的に、solverを実行する。
```
runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()
```

## runParallelPyFoam.py  
damBreakWithObstacle tutorialをpyFoamで並列計算を実行するプログラム。
このコードでは最初にPyFoamの中のParsedParameterFileというライブラリとBasicRunnerというライブラリを読み出している。
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
```

このParsedParameterFileというライブラリでは、OpenFOAMの設定ファイル等を変更したり、読み出したりすることができる。BasicRunnerではsolverの実行に利用されるライブラリである。

最初にtutorialケースの名前を設定している。
```
caseName = 'damBreakWithObstacle'
```
次にParsedParameterFileの機能を利用し、caseNameで設定したフォルダのsystem/controlDictの場所を指定し、controlDictからapplicationを読み出している。
```
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']
```
加えて、ParsedParameterFileの機能を利用して、並列計算のプロセッサー数をdecomposeParDictから読み出す。
```
with ParsedParameterFile(caseName+'/system/decomposeParDict') as decomposeParDict:
    np = decomposeParDict.content['numberOfSubdomains']
```
次に、BasicRunnerで実行するコマンドを設定し、BasicRunnerで実行する。ここでは、保存用の0ディレクトリ(0.orig)を0ディレクトリにコピーする。
```
copyRun = BasicRunner(['cp','-r',caseName+'/0.orig',caseName+'/0'], silent=True)
copyRun.start()
```
次に、blockMeshをBasicRunnerで実行し、ベースとなる計算格子を作成する。
```
meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
meshRun.start()
```
次に、topoSetをBasicRunnerで実行し、計算領域の一部を領域指定する。
```
topoSetRun = BasicRunner(['topoSet', '-case',caseName], silent=True)
topoSetRun.start()
```
次に、topoSetで指定した領域をsubsetMeshでくり抜く。
```
subSetRun = BasicRunner(['subsetMesh','-overwrite','c0','-patch','walls','-case',caseName], silent=True)
subSetRun.start()
```
そして、setFieldsで初期のダム形状を作成する。
```
setFieldsRun = BasicRunner(['setFields','-case',caseName], silent=True)
setFieldsRun.start()
```
並列計算を実行するので、decomposeParで計算領域を分割する。
```
DecomposeRun=BasicRunner(argv=["decomposePar","-case",caseName],logname="Decompose",silent=True)
DecomposeRun.start()
```
準備ができたので、並列計算を実行する。
```
runner = BasicRunner(['mpirun','-np',str(np),app,'-case',caseName,'-parallel'], silent=True)
runner.start()
```
計算終了後、reconstructMeshで計算領域を最初に再構築する。damBreakWithObstacleでは局所計算格子を細分化するAdaptive Mesh RefinementがdynamicMeshDictによって設定されている。このため、計算格子を最初に再構築する必要がある。
```
ReconstructMeshRun=BasicRunner(argv=["reconstructParMesh","-case",caseName],logname="ReconstructMesh",silent=True)
ReconstructMeshRun.start()
```
最後に変数データをreconstructParで再構築する。
```
ReconstructRun=BasicRunner(argv=["reconstructPar","-case",caseName],logname="Reconstruct",silent=True)
ReconstructRun.start()
```


## runVariousDamShapePyFoam.py    
damBreakWithObstacle tutorialをpyFoamでダム形状を変化させて逐次実行するプログラム。
このコードでは最初にPyFoamの中のParsedParameterFile, BasicRunner, SolutionDirectoryというライブラリ、numpyを読み出している。
```
import numpy as np
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Execution.BasicRunner import BasicRunner
```
プログラムの最初にダム形状の変数を設定する。
```
x_min = 0.6
x_max = 0.8
x_step = 2
y_min = 0.1875
y_max = 0.25
y_step = 2
z_min = 0.5
z_max = 0.75
z_step = 2
x_dam = np.linspace(x_min,x_max,x_step)
y_dam = np.linspace(y_min,y_max,y_step)
z_dam = np.linspace(z_min,z_max,z_step)
```
利用するプロセッサー数を変数として指定する。
```
nProc = 16
```
テンプレートとなるケースをSolutionDirectoryで指定する。
```
templateCase = SolutionDirectory('damBreakWithObstacle', archive=None)
```
OpenFOAMのdamBreakWithObstacleでは最初に保存用の0.origしかないため、0.origを0にコピーする。
```
copyRun = BasicRunner(['cp','-r','damBreakWithObstacle/0.orig','damBreakWithObstacle/0'], silent=True)
copyRun.start()
```
ダムの形状に応じたループを開始する。
```
for ix in x_dam:
   for iy in y_dam:
      for iz in z_dam:
```
新しいケースディレクトリを作成する。この際に、ループに応じた変数をケース名として設定する。
```
         case = templateCase.cloneCase('damBreakWithObstacle-x_y_z_' + str(ix) + '_' + str(iy) + '_' + str(iz))
         caseName = 'damBreakWithObstacle-x_y_z_' + str(ix) + '_' + str(iy) + '_' + str(iz)
```
ケース名を直接出力しておく。
```
         print(caseName)
```
solver名をParsedParameterFileで読み込む。
```
         with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
            app = controlDict.content['application']
```
setFieldsDictを編集し、ダムの形状設定ファイルを書き換える。
```
         with ParsedParameterFile(caseName+'/system/setFieldsDict') as setFieldsDict:
            setFieldsDict["regions"] = ['boxToCell', {'box': '(0 0 0)' '('+str(ix)+' '+str(iy)+' '+str(iz)+')', 'fieldValues': ['volScalarFieldValue', 'alpha.water', 1]}]
            setFieldsDict.writeFile()
```
decomposeParDict内に書かれている領域分割数であるnumberOfSubdomainsを書き換え、並列計算時の分割数を設定する。
```
         with ParsedParameterFile(caseName+'/system/decomposeParDict') as decomposeParDict:
            decomposeParDict["numberOfSubdomains"] = str(nProc)
            decomposeParDict.writeFile()
```
blockMeshをBasicRunnerで設定し、blockMeshを実行する。
```
         meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
         meshRun.start()
```
topoSetで局所領域を指定する。
```
         topoSetRun = BasicRunner(['topoSet', '-case',caseName], silent=True)
         topoSetRun.start()
```
subSetMeshでtopoSetで指定した領域を計算領域からくり抜く。
```
         subSetRun = BasicRunner(['subsetMesh','-overwrite','c0','-patch','walls','-case',caseName], silent=True)
         subSetRun.start()
```
setFieldsで先ほど書き換えたsetFiledsDictを読み込んでダムの形状になるように体積分率alphaの初期値を更新する。
```
         setFieldsRun = BasicRunner(['setFields','-case',caseName], silent=True)
         setFieldsRun.start()
```
decomposeParによって先ほど書き換えたdecomposeParDictを読み込んで、計算領域を分割する。
```
         DecomposeRun=BasicRunner(argv=["decomposePar","-case",caseName],logname="Decompose",silent=True)
         DecomposeRun.start()
```
solverを実行し、並列計算を行う
```
         runner = BasicRunner(['mpirun','-np',str(nProc),app,'-case',caseName,'-parallel'], silent=True)
         runner.start()
```
領域分割された計算結果をreconstructParMeshで再構築する。AMRを利用しているので、最初に計算格子を再構築する。
```
         ReconstructMeshRun=BasicRunner(argv=["reconstructParMesh","-case",caseName],logname="ReconstructMesh",silent=True)
         ReconstructMeshRun.start()
```
最後に変数データをreconstructParで再構築する。
```
         ReconstructRun=BasicRunner(argv=["reconstructPar","-case",caseName],logname="Reconstruct",silent=True)
         ReconstructRun.start()
```
