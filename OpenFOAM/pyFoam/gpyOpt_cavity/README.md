# READ ME  
このディレクトリ内のプログラムを実行する前に、[GPyOpt](https://sheffieldml.github.io/GPyOpt/)ライブラリ、[SciPy](https://scipy.org)をインストールする必要がある。PyPIを利用してSciPy, GPyOptを最初にインストールしておく。  
```
pip install scipy
pip install gpyopt
```
加えて、pythonライブラリであるpandasも利用する。こちらも同様にPyPIを利用してインストールしておく。
```
pip install pandas
```
もし、GpyOptの可視化ライブラリまで利用する場合はmatplotlibライブラリまでをインストールする必要がある。この場合のmaplotlibは古いversionでしか対応していないため、古いmatplotlibのバージョンを指定したインストールを行う。  
```
pip install matplotlib==3.1.3
```

このチュートリアルではOpenFOAMのtutorialであるcavityClippedを少し修正したものを利用し、GpyOptを利用して最も最適となる条件を探索する。cavityの計算領域の一部を切り取るcavityClippedであるが、この切り取る領域を変更し、元々のcavity計算領域の計算中央における上下方向速度が最も下向きに強くなる条件を探索する。このプログラムは基本的にはscipy_optimize_cavityの拡張版になるので、そちらをまず参考とすること。    


## findGPyOptFoam.py  
最初にPyFoam, GPyOpt, pandas, matplotlibを読み込む。   
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory

import GPyOpt
from GPyOpt.methods import BayesianOptimization

import pandas as pd

import matplotlib.pyplot as plt
```
関数func_uy_centerの中身はscipy_optimize_cavityの場合とほぼ同じなので説明を省略する。  


GpyOptではdomainでname, type, domainで探索範囲や名前を指定する。  
```
domain = [{'name':'xclip','type':'continuous', 'domain':(0.3,0.9)},
          {'name':'yclip','type':'continuous', 'domain':(0.1,0.45)}]
```
BayesianOptimizationを読み込み、最適化する関数、先ほど指定したdomain、獲得関数等を読み込む。GpyOptは様々な獲得関数を指定できる。詳細はGpyOptの[マニュアル](https://gpyopt.readthedocs.io/en/latest/)を参照することとするが、以下のものを利用できる。  

* EI: Expected Improvement 
* EI_mcmc: Expected Improvement, MCMC (Markov Chain Monte Carlo) 
* LCB: Lower Confidence Bound 
* LCB_mcmc: Lower Confidence Bound, MCMC (Markov Chain Monte Carlo)
* LP: Local Penalization
* MPI: Maximum Probability of Improvement
* MPI_mcmc: Maximum Probability of Improvement, MCMC (Markov Chain Monte Carlo) 

MCMCを利用する場合は、model_typeを追加指定し、MCMC系のものを利用する必要がある。  

```
optimizer = BayesianOptimization(
    f=func_uy_center,
    domain=domain,
    acquisition_type='EI'
)
```
最終的に、最適化を実行する。max_iterで探索数を指定する。  
```
optimizer.run_optimization(max_iter=20)
```
続いて、matplotlibの機能を利用して、結果を可視化する。  
```
optimizer.plot_acquisition()

plt.savefig('fig.png')
plt.close()
```
最適(最小)値になるようにどのように値が収束しているかを可視化する。  
```
optimizer.plot_convergence()
plt.savefig('figConvergence.png')
plt.close()
```
最終的に、最適値を出力し、各結果を出力する。  
```
print("The optimum condition is: ")
print(optimizer.fx_opt, optimizer.x_opt)
optimizer.save_evaluations("evaluation.txt")
optimizer.save_report("finalReport.txt")
```

