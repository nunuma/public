from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory

import GPyOpt
from GPyOpt.methods import BayesianOptimization

import pandas as pd

import matplotlib.pyplot as plt

templateCase = SolutionDirectory("cavityClipped", archive=None)

def func_uy_center(x):
   xclip = x[-1,0]
   yclip = x[-1,1]
   # define case
   caseName = "cavityClipped_xclip_" + str(xclip) + "_yclip_" + str(yclip)
   case = templateCase.cloneCase(caseName)
   
   # read the solver name
   with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
       app = controlDict.content['application']

   # change the blockMesh file
   with ParsedParameterFile(case.name+"/system/blockMeshDict") as blockMeshDict:
      blockMeshDict["xclip"] = xclip
      blockMeshDict["yclip"] = yclip
      nCell = blockMeshDict["nCell"]
      blockMeshDict["nx"] = int(xclip*nCell)
      blockMeshDict["nxr"] = int(nCell - xclip*nCell)
      blockMeshDict["ny"] = int(yclip*nCell)
      blockMeshDict["nyr"] = int(nCell - yclip*nCell)
      blockMeshDict.writeFile()

   # execute blockMesh
   meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
   meshRun.start()

   # run solver
   runner = BasicRunner([app,'-case',caseName], silent=True)
   runner.start()

   # define file name of calculated data
   fileNamePost = caseName+'/postProcessing/probes/0/U'
   df = pd.read_table(fileNamePost, header=None, skiprows=3, sep='\s+')

   # return uy at the center of calculation domain
   print("uy at the center of calculation domain is: "+str(df[2].iloc[-1]))
   print("xclip, yclip are :"+str(xclip)+" "+str(yclip))
   return df[2].iloc[-1]

domain = [{'name':'xclip','type':'continuous', 'domain':(0.3,0.9)},
          {'name':'yclip','type':'continuous', 'domain':(0.1,0.45)}]

optimizer = BayesianOptimization(
    f=func_uy_center,
    domain=domain,
    acquisition_type='EI'
)

optimizer.run_optimization(max_iter=20)
optimizer.plot_acquisition()

plt.savefig('fig.png')
plt.close()

optimizer.plot_convergence()
plt.savefig('figConvergence.png')
plt.close()

print("The optimum condition is: ")
print(optimizer.fx_opt, optimizer.x_opt)
optimizer.save_evaluations("evaluation.txt")
optimizer.save_report("finalReport.txt")
