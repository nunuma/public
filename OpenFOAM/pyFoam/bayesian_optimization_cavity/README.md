# READ ME  
このディレクトリ内のプログラムを実行する前に、[bayesian-optimization](https://github.com/bayesian-optimization/BayesianOptimization)ライブラリをインストールする必要がある。PyPIを利用してbayesian-optimizationを最初にインストールしておく。  
```
pip install bayesian-optimization
```
加えて、pythonライブラリであるpandasも利用する。こちらも同様にPyPIを利用してインストールしておく。
```
pip install pandas
```

このチュートリアルではOpenFOAMのtutorialであるcavityClippedを少し修正したものを利用し、bayesian-optimizationを利用して最も最適となる条件を探索する。cavityの計算領域の一部を切り取るcavityClippedであるが、この切り取る領域を変更し、元々のcavity計算領域の計算中央における上下方向速度が最も下向きに強くなる条件を探索する。このプログラムは基本的にはscipy_optimize_cavityの拡張版になるので、そちらをまず参考とすること。    


## findBayesianPyFoam.py  
cavityClippedのtutorialをpyFoamで実行し、計算領域中心点におけるy方向速度を最小になる条件をbayesian-optimizationで探索するプログラム  
基本的に関数化した中身はほぼscipy_optimize_cavityのrunFunctionPyFoam.pyと同じためその部分の説明は省略する。  

func_uy_centerでは以下のようにuyの負の値を返すように関数を変更する。これは、bayesian-optimizationのライブラリでは最大値を探索することになるためである。  
```
   return -df[2].iloc[-1]
```
探索範囲を設定する。  
```
pbounds = {'xclip': (0.3, 0.9), 'yclip': (0.1, 0.45)} 
```
bayesianOptimizationの機能で、最適化する関数をf=で読み込み、pbounds=で探索範囲を読み込む。  
```
optimizer = BayesianOptimization(
    f=func_uy_center,
    pbounds=pbounds,
    random_state=1,
)
```
最大値を探索する。初期ランダム探索数(init_points)を5に設定し、探索回数(n_iter)を20と設定している。  
```
optimizer.maximize(
    init_points=5,
    n_iter=20,
)
```
最適値を出力する。   
```
print(optimizer.max)
```

## findBayesianUCBPyFoam.py  
cavityClippedのtutorialをpyFoamで実行し、計算領域中心点におけるy方向速度を最小になる条件をbayesian-optimizationで探索するプログラム  
findBayesianPyFoam.pyの獲得関数をupper confidence bound (ucb)に変更した場合のプログラム  
変更点は以下の箇所  
```
optimizer.maximize(
    init_points=5,
    n_iter=20,
    acq='ucb'
)
```
ここで、acqで獲得関数の種類を設定する。このbayesian-optimizationでは、獲得関数の種類として、  

* ucb: Upper Confidence  
* ei: Expected Improvement  
* poi: Probability Of Improvement  

の3種類を指定できる。何も記入しない場合には、eiを利用していることになる。  
詳細な設定に関しては以下のURLを参照。  
[URL](https://github.com/bayesian-optimization/BayesianOptimization/blob/master/examples/exploitation_vs_exploration.ipynb).  


