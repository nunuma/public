# import classes from python library
import numpy as np
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Basics.DataStructures import Vector

# set the sliding velocity
v_min = 0.1
v_max = 0.5
step = 5
uslide = np.linspace(v_min,v_max,step)
print(uslide)

# identify the template case
templateCase = SolutionDirectory("cavity", archive=None)

# roop with various parameter
for ub in uslide:
   
   # create new case 
   case = templateCase.cloneCase('cavity-u' + str(ub) + 'ms')
   caseName = 'cavity-u' + str(ub) + 'ms'
   print(caseName)

   # access and change the velocity boundary condition
   uBC = ParsedParameterFile(case.name+"/0/U")
   uBC["boundaryField"]["movingWall"]["value"].setUniform(Vector(ub,0,0))
   uBC.writeFile()

   # Mesh generation
   MeshRun=BasicRunner(argv=["blockMesh","-case",caseName],logname="Mesh")
   MeshRun.start()

   # Run solver
   Run=BasicRunner(argv=["icoFoam","-case",caseName],silent=True,logname="Solver")
   Run.start()
