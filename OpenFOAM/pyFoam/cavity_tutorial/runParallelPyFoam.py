from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner

caseName = 'cavity'

# Mesh generation
MeshRun=BasicRunner(argv=["blockMesh","-case",caseName],logname="Mesh",silent=True)
MeshRun.start()

# decompose the calculation domain 
DecomposeRun=BasicRunner(argv=["decomposePar","-case",caseName],logname="Decompose",silent=True)
DecomposeRun.start()

# run solver in parallel
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']

print(app)
runner = BasicRunner(['mpirun -np 2',app,'-case',caseName,'-parallel'], silent=True,logname="Solver")
runner.start()

# reconstruct the calculation domain
ReconstructRun=BasicRunner(argv=["reconstructPar","-case",caseName],logname="Reconstruct",silent=True)
ReconstructRun.start()
