from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Execution.BasicRunner import BasicRunner

caseName = 'cavity'
solver = 'icoFoam'

directory = SolutionDirectory(caseName)
directory.clearResults()

runner = BasicRunner([solver,'-case',caseName], silent=True)
runner.start()
