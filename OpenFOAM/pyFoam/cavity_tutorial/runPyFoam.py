from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner

caseName = 'cavity'

with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']

print(app)
runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()
