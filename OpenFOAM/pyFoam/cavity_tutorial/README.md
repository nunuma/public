# READ ME  


## runPyFoam.py  
cavity tutorialをpyFoamで実行するプログラム。  
このコードでは最初にPyFoamの中のParsedParameterFileというライブラリとBasicRunnerというライブラリを読み出している。  
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
```

このParsedParameterFileというライブラリでは、OpenFOAMの設定ファイル等を変更したり、読み出したりすることができる。BasicRunnerではsolverの実行に利用されるライブラリである。  

最初にtutorialケースの名前を設定している。
```
caseName = 'cavity'
```

次にParsedParameterFileの機能を利用し、caseNameで設定したフォルダのsystem/controlDictの場所を指定し、controlDictからapplicationを読み出している。  
```
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']
```
最終的に、BasicRunnerで実行するコマンドを設定し、最終的にsolverをBasicRunnerで実行する。ここでは、OpenFOAMのsolverのオプションである-caseを利用して実行先のケースディレクトリを指定している。   

```
runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()
```

## runPyFoamSolution.py
cavity tutorialをpyFoamで実行するプログラム。
このコードでは最初にPyFoamの中のSolutionDirectoryとBasicRunnerというライブラリを読み出している。
```
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Execution.BasicRunner import BasicRunner
```
このコードでは最初に実行するケースディレクトリとsolverを変数で指定する。  
```
caseName = 'cavity'
solver = 'icoFoam'
```
その後、clearResultsによって計算結果を初期化する。
```
directory = SolutionDirectory(caseName)
directory.clearResults()
```
最後に、BasicRunnerでsolverを実行する。
```
runner = BasicRunner([solver,'-case',caseName], silent=True)
runner.start()
```


## cleanPyFoam.py
実行したcavity tutorialの計算結果を消去する。以下にpythonコードを示す。  
このコードではpyFoamのClearCaseというライブラリを最初に読み出している。  
```
from PyFoam.Applications.ClearCase import ClearCase
```
その後、caseNameという変数を定義し、実行したtutorialケースの名前を読み込んでいる。  
```
caseName = 'cavity'
```
最終的にClearCaseを実行することで、計算結果のデータを消去する。  
```
runner = ClearCase(caseName)
```


## runParallelPyFoam.py
cavityのtutorialを並列実行する。  

このコードではpyFoamのParsedParameterFile, BasicRunnerというライブラリを最初に読み込んでいる。  
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
```
runPyFoam.pyと同様にその後、caseNameという変数を定義する。
```
caseName = 'cavity'
```
最初にBasicRunnerでblockMeshを実行し、計算格子を作成する。その後、basicRunnerでdecomposeParを実行し、領域分割を行う。  
```
MeshRun=BasicRunner(argv=["blockMesh","-case",caseName],logname="Mesh",silent=True)
MeshRun.start()
DecomposeRun=BasicRunner(argv=["decomposePar","-case",caseName],logname="Decompose",silent=True)
DecomposeRun.start()
```
ParsedParameterFileで実行するプログラムをsystem/controlDict内のapplicationから読み込み、BasicRunnerで並列実行を行う。
```
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']

runner = BasicRunner(['mpirun -np 2',app,'-case',caseName,'-parallel'], silent=True,logname="Solver")
runner.start()
```
並列計算を行った後にBasicRunnerのreconstructParで領域再構成を行う。  
```
ReconstructRun=BasicRunner(argv=["reconstructPar","-case",caseName],logname="Reconstruct",silent=True)
ReconstructRun.start()
```



## runMultiplePyFoam.py   
cavityの条件を修正し、逐次cavityのtutorialを実行する。(参考にした[資料](https://wikis.ovgu.de/lss/lib/exe/fetch.php?media=guide:presentation:cfd_meeting_pyfoam.pdf)。)   

このコードではpyFoamのParsedParameterFile, BasicRunner, SolutionDirectory, Vectorというライブラリとnumpyを最初に読み出している。 
```
import numpy as np
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.Basics.DataStructures import Vector
```
次に、cavityの壁面速度を変更するため、最小壁面速度と最大壁面速度を定義し、その速度をどの程度分割したもので計算を実行するかを設定する。
```
v_min = 0.1
v_max = 0.5
step = 5
uslide = np.linspace(v_min,v_max,step)
```

その後、templateCaseという変数を定義し、実行するtutorialケースの名前を読み込んでいる。  
```
templateCase = SolutionDirectory("cavity", archive=None)
```
最終的にpythonのforループを利用して、壁面速度を変更したチュートリアルケースを逐次計算する。 
```
for ub in uslide:
``` 
forループ中は最初にcloneCaseを利用して新しいケースディレクトリを作成し、
```
   case = templateCase.cloneCase('cavity-u' + str(ub) + 'ms')
   caseName = 'cavity-u' + str(ub) + 'ms'
```
ParsedParameterFileを利用して0/Uの中の境界条件を更新し、
```
   uBC = ParsedParameterFile(case.name+"/0/U")
   uBC["boundaryField"]["movingWall"]["value"].setUniform(Vector(ub,0,0))
   uBC.writeFile()
```
blockMeshをBasicRunnerで実行し、
```
   MeshRun=BasicRunner(argv=["blockMesh","-case",caseName],logname="Mesh")
   MeshRun.start()
```
solverをBasicRunnerで実行する。
```
   Run=BasicRunner(argv=["icoFoam","-case",caseName],silent=True,logname="Solver")
   Run.start()
```

