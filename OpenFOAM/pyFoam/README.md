# READ ME  


pythonを用いてOpenFOAMを制御する[pyFoam](https://pypi.org/project/PyFoam/)というpythonライブラリが存在する。このライブラリではOpenFOAMのsolverのログファイルを解析したり、solverやutilityを実行したりすることができ、OpenFOAMをpythonで制御することができる。このライブラリは、Bernhard Gschaiderらが中心となって開発し、おおよそ1年おきに更新されている。ライセンスはGPL v2である。詳細な説明は[OpenFOAM wiki](http://openfoamwiki.net/index.php/Contrib/PyFoam)に記されている。pyFoamを用いた例題を以下のディレクトリに格納する。  

pyFoamがインストールされた状態でなければ動かないので、最初にpyPIを利用してpyFoamをインストールした上で実行すること。  
```
pip install PyFoam
```

* * *

以下に各種サンプルプログラムを配置している。簡単な順から上に並べていくので、初学者はREADMEの上から書かれている順に実行、勉強していくことをお勧めする。  

* * *  

## cavity_tutorial  
OpenFOAMのtutorialであるcavity(solverはicoFoam)を利用し、pyFoamでcavityを動作させるためのサンプルプログラム格納箇所  


## damBreak_tutorial
OpenFOAMのtutorialであるdamBreakWithObstacle(solverはinterFoam)を利用し、pyFoamでdamBreakWithObstacleを実行させるためのサンプルプログラム格納箇所  

## scipy_optimize_cavity
OpenFOAMのtutorialとpythonライブラリであるSciPy optimizeを組み合わせて最小値を求めるサンプルプログラム格納場所  

## bayesian_optimization_cavity  
OpenFOAMのtutorialとpythonライブラリであるbayesian-optimizationを組み合わせて最小値を求めるサンプルプログラム格納箇所  

## gpyOpt_cavity  
OpenFOAMのtutorialとpythonライブラリであるGpyOptを組み合わせて最小値を求めるサンプルプログラム格納箇所  

* * * 

## 動作確認環境
OpenFOAM-v1812, OpenFOAM-v2212  
python-3.6.8  
