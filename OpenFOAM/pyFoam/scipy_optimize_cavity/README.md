# READ ME  
このディレクトリ内のプログラムを実行する前に、SciPyライブラリをインストールする必要がある。PyPIを利用してSciPyを最初にインストールしておく。  
```
pip install scipy
```
加えて、pythonライブラリであるpandasも利用する。こちらも同様にPyPIを利用してインストールしておく。
```
pip install pandas
```

このチュートリアルではOpenFOAMのtutorialであるcavityClippedを少し修正したものを利用し、scipy.optimizeを利用して最も最適となる条件を探索する。cavityの計算領域の一部を切り取るcavityClippedであるが、この切り取る領域を変更し、元々のcavity計算領域の計算中央における上下方向速度が最も下向きに強くなる条件を探索する。  

## runPyFoam.py 
cavityClippedのtutorialをpyFoamで実行し、計算領域中心点におけるy方向速度を出力するプログラム。  
このコードでは最初にPyFoamの中のParsedParameterFileというライブラリとBasicRunnerというライブラリ、pandasというライブラリを読み出している。  
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
import pandas as pd
```

最初にtutorialケースの名前を設定している。  
```
caseName = 'cavityClipped'
```
次にParsedParameterFileの機能を利用し、caseNameで設定したフォルダのsystem/controlDictの場所を指定し、controlDictからapplicationを読み出している。  
```
with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']
```
最終的に、BasicRunnerで実行するコマンドを設定し、最終的にsolverをBasicRunnerで実行する。ここでは、OpenFOAMのsolverのオプションである-caseを利用して実行先のケースディレクトリを指定している。  
```
runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()
```
次に、pandasを用いてファイルから数値を読み出す。今回のcavityClippedではsystem/controlDict内にprobesの設定を追記しているため、postProcesing/probes/0/Uというファイルが計算実行後に作成される。これは計算領域中央点における速度の時系列データである。この時系列データの最終時刻のy方向速度を取得するため、最初に読み込むファイル名を指定する。  
```
fileNamePost = caseName+'/postProcessing/probes/0/U'
```
そして、pandasのread_tableでファイルを読み込む。  
```
df = pd.read_table(fileNamePost, header=None, skiprows=3, sep='\s+')
```

最終的に、読み込んだファイルの最終行のy方向の速度データを出力する。  
```
print("Uy at the center of calculation domain is : "+str(df[2].iloc[-1]))
```



## cleanPyFoam.py  
実行したcavity tutorialの計算結果を消去する。以下にpythonコードを示す。
このコードではpyFoamのClearCaseというライブラリを最初に読み出している。
```
from PyFoam.Applications.ClearCase import ClearCase
```
その後、caseNameという変数を定義し、実行したtutorialケースの名前を読み込んでいる。
```
caseName = 'cavityClipped'
```
最終的にClearCaseを実行することで、計算結果のデータを消去する。
```
runner = ClearCase(caseName)
``` 


## runFunctionPyFoam.py 
cavityClippedのtutorialをpyFoamで実行し、計算領域中心点におけるy方向速度を出力するプログラム。
runPyFoam.pyとは異なり、clipped領域を読み込むことができる関数に変更  
このコードでは最初にPyFoamの中のParsedParameterFile, BasicRunner, SolutionDirectoryというライブラリ、pandasというライブラリを読み出している。
```
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
import pandas as pd
```

SolutionDirectoryを用いて、templateCaseを作成する。  
```
templateCase = SolutionDirectory("cavityClipped", archive=None)
```

xlipとyclipを読み込むように関数化する。   
```
def func_uy_center(xclip,yclip):
```
clip領域に合わせてケースディレクトリを作成する。  
```
   caseName = "cavityClipped_xclip_" + str(xclip) + "_yclip_" + str(yclip)
   case = templateCase.cloneCase(caseName)
```
solver名を読み込む。  
```
   with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
       app = controlDict.content['application']
```
blockMeshDictを書き換える。  
```
   with ParsedParameterFile(case.name+"/system/blockMeshDict") as blockMeshDict:
      blockMeshDict["xclip"] = xclip
      blockMeshDict["yclip"] = yclip
      nCell = blockMeshDict["nCell"]
      blockMeshDict["nx"] = int(xclip*nCell)
      blockMeshDict["nxr"] = int(nCell - xclip*nCell)
      blockMeshDict["ny"] = int(yclip*nCell)
      blockMeshDict["nyr"] = int(nCell - yclip*nCell)
      blockMeshDict.writeFile()
```
blockMeshを実行する。  
```
   meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
   meshRun.start()
```
solverを実行する。   
```
   runner = BasicRunner([app,'-case',caseName], silent=True)
   runner.start()
```
次に、pandasを用いてファイルから数値を読み出す。今回のcavityClippedではsystem/controlDict内にprobesの設定を追記しているため、postProcesing/probes/0/Uというファイルが計算実行後に作成される。これは計算領域中央点における速度の時系列データである。この時系列データの最終時刻のy方向速度を取得するため、最初に読み込むファイル名を指定する。  
```
   fileNamePost = caseName+'/postProcessing/probes/0/U'
```
そして、pandasのread_tableでファイルを読み込む。  
```
   df = pd.read_table(fileNamePost, header=None, skiprows=3, sep='\s+')
```
最後に、uyを出力し、関数に値を返す。  
```
   print("uy at the center of calculation domain is: "+str(df[2].iloc[-1]))
   return df[2].iloc[-1]
```
関数に値を代入して実行する。
```
func_uy_center(0.4,0.2)
```

## findOptimumPyFoam.py  
cavityClippedのtutorialをpyFoamで実行し、計算領域中心点におけるy方向速度を最小になる条件をSciPyのoptimizeで探索するプログラム  
基本的に関数化した中身はほぼrunFunctionPyFoam.pyと同じためその部分の説明は省略する。  
SciPyのoptimizeのBoundsを利用して、データ範囲を決める。  
```
bounds = Bounds([0.3, 0.1],[0.9, 0.45])
```   
optimizeのSLSQPメソッドで最適化する。初期条件は[0.4,0.2]であるとする。  
```
res = optimize.minimize(func_uy_center, x0=[0.4,0.2], bounds=bounds, method='SLSQP')
```
最適条件を出力する。
```
print(res.x)
```
