from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
import pandas as pd

caseName = 'cavityClipped'

with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
    app = controlDict.content['application']

# execute blockMesh
meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
meshRun.start()

runner = BasicRunner([app,'-case',caseName], silent=True)
runner.start()

# define file name of calculated data
fileNamePost = caseName+'/postProcessing/probes/0/U'
df = pd.read_table(fileNamePost, header=None, skiprows=3, sep='\s+')

# print Uy at the center location of calculation domain at the end of simulation
print("Uy at the center of calculation domain is : "+str(df[2].iloc[-1]))
