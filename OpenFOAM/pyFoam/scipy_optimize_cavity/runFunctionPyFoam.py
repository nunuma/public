from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
import pandas as pd

templateCase = SolutionDirectory("cavityClipped", archive=None)

def func_uy_center(xclip,yclip):
   # define case
   caseName = "cavityClipped_xclip_" + str(xclip) + "_yclip_" + str(yclip)
   case = templateCase.cloneCase(caseName)
   
   # read the solver name
   with ParsedParameterFile(caseName+'/system/controlDict') as controlDict:
       app = controlDict.content['application']

   # change the blockMesh file
   with ParsedParameterFile(case.name+"/system/blockMeshDict") as blockMeshDict:
      blockMeshDict["xclip"] = xclip
      blockMeshDict["yclip"] = yclip
      nCell = blockMeshDict["nCell"]
      blockMeshDict["nx"] = int(xclip*nCell)
      blockMeshDict["nxr"] = int(nCell - xclip*nCell)
      blockMeshDict["ny"] = int(yclip*nCell)
      blockMeshDict["nyr"] = int(nCell - yclip*nCell)
      blockMeshDict.writeFile()

   # execute blockMesh
   meshRun = BasicRunner(['blockMesh', '-case',caseName], silent=True)
   meshRun.start()

   # run solver
   runner = BasicRunner([app,'-case',caseName], silent=True)
   runner.start()

   # define file name of calculated data
   fileNamePost = caseName+'/postProcessing/probes/0/U'
   df = pd.read_table(fileNamePost, header=None, skiprows=3, sep='\s+')

   # return uy at the center of calculation domain
   print("uy at the center of calculation domain is: "+str(df[2].iloc[-1]))
   return df[2].iloc[-1]

func_uy_center(0.4,0.2)
