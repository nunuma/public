    fvVectorMatrix UEqn
    (
        fvm::ddt(rho, U)
      + fvm::div(rhoPhi, U)
      + turbulence->divDevRhoReff(rho, U)
     ==
        sources(rho, U)
    );

    UEqn.relax();
    sources.constrain(UEqn);

    if (pimple.momentumPredictor())
    {
        solve
        (
            UEqn
         ==
            fvc::reconstruct
            (
                (
//                    fvc::interpolate(interface.sigmaK())*fvc::snGrad(alpha1)
                    interface.sigma()*fvc::snGrad(psi)*fvc::interpolate(C)*fvc::interpolate(delta)
                  - ghf*fvc::snGrad(rho)
                  - fvc::snGrad(p_rgh)
                ) * mesh.magSf()
            )
        );
    }
