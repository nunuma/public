# README 

# ##########################################

#### OpenFOAM solvers for v2212 implemented by Takuya YAMAMOTO  
  
   
## helmholtzFoam  

This solver solves Helmholtz equation for acoustic wave propagation.   
The implementation is the same way as written in Ph.D. thesis of Rashid Jamshidi at Clausthal University of Technology, Germany (2013).   
  
  
## helmholtzBubbleFoam

This solver solves Helmholtz equation for acoustic wave propagation.  
The implementation is the same way as written in Ph.D. thesis of Rashid Jamshidi at Clausthal University of Technology, Germany (2013).  
In this solver, spherical bubbles are uniformly distributed with linear oscillation in accordance with acoustic pressure wave.  
  
  
## How to compile

Please compile these solvers in the following procedures.
  

1. Move the solvers into $WM_PROJECT_USER_DIR/applications/solvers
  
2. Change the current directory into the solver folder that you want to compile.  
  
3. wclean
 
4. wmake
